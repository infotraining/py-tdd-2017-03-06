class BowlingGame:
    NUMBER_OF_PINS = 10
    ROLLS_IN_FRAME = 2
    NUMBER_OF_FRAMES = 10

    def __init__(self):
        self.rolls = []

    def roll(self, pins):
        self.rolls.append(pins)

    def score(self):
        frame_index = 0
        total = 0
        for _ in range(self.NUMBER_OF_FRAMES):
            is_strike = self.rolls[frame_index] == self.NUMBER_OF_PINS
            if is_strike:
                total += sum(self.rolls[frame_index:frame_index+1+self.ROLLS_IN_FRAME])
                frame_index += 1
            else:
                frame_sum = sum(self.rolls[frame_index:frame_index+self.ROLLS_IN_FRAME])
                is_spare = frame_sum == self.NUMBER_OF_PINS
                bonus = self.rolls[frame_index+self.NUMBER_OF_PINS] if is_spare else 0
                total += frame_sum + bonus
                frame_index += self.ROLLS_IN_FRAME
        return total
