import unittest
from bowling_game import BowlingGame

class BowlingGameTests(unittest.TestCase):
    def setUp(self):
        self.game = BowlingGame()

    def _roll_many(self, pins, rolls):
        for i in range(rolls):
            self.game.roll(pins)

    def _roll_spare(self):
        self.game.roll(4)
        self.game.roll(6)

    def test_when_all_throws_in_gutter_then_score_should_be_zero(self):
        self._roll_many(pins=0, rolls=20)
        self.assertEqual(self.game.score(), 0)

    def test_when_no_mark_score_then_score_should_be_sum_of_pins(self):
        self._roll_many(pins=3, rolls=20)
        self.assertEqual(self.game.score(), 60)

    def test_when_spare_then_next_roll_is_counted_twice(self):
        self._roll_spare()  # 10 points + bonus for next roll (+3)
        self._roll_many(pins=3, rolls=18)  # 18 * 3 points
        self.assertEqual(self.game.score(), 10 + 3 + 3*18)

    # def test_(self):
    #     self._roll_many(pins=5, rolls=21)
    #     self.assertEqual(self.game.score(), 20*5 + 10*5)

    def test_when_strike_then_next_two_rolls_are_counted_twice(self):
        self.game.roll(10)  # 10 points + bonus for next two rolls (+6)
        self._roll_many(pins=3, rolls=18)  # 18 * 3 points
        self.assertEqual(self.game.score(), 10 + 6 + 3*18)

    # def test_when_perfect_game_then_score_should_be_300(self):
    #     self._roll_many(pins=10, rolls=12)
    #     self.assertEqual(self.game.score(), 300)
