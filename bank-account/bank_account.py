INTEREST_RATE = 0.1


class BankAccount:
    '''
    Test bla
    >>> b = BankAccount(id=1234)
    >>> b.state
    'Normal'
    '''

    def __init__(self, id):
        self.balance = 0
        self.id = id

    def deposit(self, amount):
        self.balance += amount

    def withdraw(self, amount):
        self.balance -= amount

    def pay_interest(self):
        self.balance += self.balance * INTEREST_RATE

    @property
    def state(self):
        return 'Normal' if self.balance >= 0 else 'Overdraft'
