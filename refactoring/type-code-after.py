from contextlib import redirect_stdout
import io
import unittest


class Shape:
    def draw(self):
        print(self.character)

class Circle(Shape):
    shape = 'circle'
    character = 'o'

class Dot(Shape):
    shape = 'dot'
    character = '.'
    

class ShapeTests(unittest.TestCase):
    def test_circle(self):
        self._test(shape=Circle(),
                   expected_output='o')

    def test_dot(self):
        self._test(shape=Dot(),
                   expected_output='.')

    def _test(self, shape, expected_output):
        f = io.StringIO()
        with redirect_stdout(f):
            shape.draw()
        output = f.getvalue()
        self.assertEqual(output, expected_output+'\n')


if __name__ == "__main__":
    unittest.main()
