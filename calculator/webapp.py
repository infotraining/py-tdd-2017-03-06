from flask import Flask, request

import calc

app = Flask(__name__)

HOME_PAGE = '''
<h1>Home Page</h1>
<form method="POST">
<input type="text" name="first" /> +
<input type="text" name="second" /> =
<input type="submit" value="add" />
</form>
'''

@app.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        first = float(request.form['first'])
        second = float(request.form['second'])
        result = calc.add(first, second)
        return '{} + {} = {}'.format(first, second, result)
    else:
        return HOME_PAGE

if __name__ == "__main__":
    app.run()
