from behave import *

@step('I navigate to home page')
def step_impl(context):
    context.resp = context.client.get('/')

@then('{text} should be displayed')
def step_impl(context, text):
    assert text in context.resp

# @then('Home Page should be displayed')
# def step_impl(context):
#     assert 'Home Page' in context.resp

@given('I enter {number} as {field} number')
def step_impl(context, number, field):
    context.resp.form[field] = number

# @given('I enter 50 as first number')
# def step_impl(context):
#     context.resp.form['first'] = '50'

# @given('I enter 70 as second number')
# def step_impl(context):
#     context.resp.form['second'] = '70'

@when('I press add')
def step_impl(context):
    context.resp = context.resp.form.submit()

# @then('50.0 + 70.0 = 120.0 should be displayed')
# def step_impl(context):
#     assert '50.0 + 70.0 = 120.0' in context.resp
