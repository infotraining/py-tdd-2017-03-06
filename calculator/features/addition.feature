Feature: add numbers
    In order to avoid silly mistakes
    As a math idiot
    I want to be told the sum of two numbers

    Scenario: Navigation to Home Page
         When I navigate to home page
         Then Home Page should be displayed

    Scenario: Add two numbers
        Given I navigate to home page
          And I enter 50 as first number
          And I enter 70 as second number
         When I press add
         Then 50.0 + 70.0 = 120.0 should be displayed

    Scenario: Add two numbers
        Given I navigate to home page
          And I enter 20 as first number
          And I enter 30 as second number
         When I press add
         Then 20.0 + 30.0 = 50.0 should be displayed 

    @outline @wip
    Scenario Outline: Add two numbers
        Given I navigate to home page
          And I enter <first> as first number
          And I enter <second> as second number
         When I press add
         Then <expected> should be displayed     

    Examples: valid data
        | first | second | expected            |
        | 50    | 70     | 50.0 + 70.0 = 120.0 |
        | 20.0  | 30.0   | 20.0 + 30.0 = 50.0  | 

    Examples: invalid data
        | first | second | expected            |
        | 50asd |        | invalid data        |
