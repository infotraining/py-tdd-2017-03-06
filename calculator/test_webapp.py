from unittest.mock import Mock, patch
from nose2.tools import such

import webapp

with such.A('Home Page') as it:
    with it.having('adding form'):
        @it.should('delegate request parameters to calc.add')
        @patch('webapp.request')
        @patch('webapp.calc')
        def test(calc_mock, request_mock):
            request_mock.method = 'POST'
            request_mock.form = {
                'first': '50',
                'second': '70',
            }

            webapp.home()

            calc_mock.add.assert_called_once_with(50.0, 70.0)

    it.createTests(globals())
