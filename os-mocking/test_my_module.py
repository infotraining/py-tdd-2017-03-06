import os.path
import unittest

from unittest import mock
# import mock

from my_module import rm


class NoMocking(unittest.TestCase):
    def test_provided_extension_should_be_used(self):
        filename = 'file.md'
        open(filename, 'w').close()
        rm(filename)
        self.assertFalse(os.path.exists(filename))

    def test_when_extension_is_missing_then_use_default_one(self):
        filename = 'file.txt'
        open(filename, 'w').close()
        rm('file')
        self.assertFalse(os.path.exists(filename))


class Patching(unittest.TestCase):
    @mock.patch('my_module.os')
    # @mock.patch('my_module.remove')
    def test_provided_extension_should_be_used(self, os_mock):  # remove_mock
        rm('file.md')
        os_mock.remove.assert_called_once_with('file.md')
        # remove_mock.assert_called_once_with('file.md')

    @mock.patch('my_module.os')
    def test_when_extension_is_missing_then_use_default_one(self, os_mock):
        rm('file')
        os_mock.remove.assert_called_once_with('file.txt')

@mock.patch('my_module.os')
class ClassPatching(unittest.TestCase):
    def test_provided_extension_should_be_used(self, os_mock):
        rm('file.md')
        os_mock.remove.assert_called_once_with('file.md')

    def test_when_extension_is_missing_then_use_default_one(self, os_mock):
        rm('file')
        os_mock.remove.assert_called_once_with('file.txt')


class PatchingInSetup(unittest.TestCase):
    def setUp(self):
        os_patcher = mock.patch('my_module.os')
        self.os_mock = os_patcher.start()
        self.addCleanup(self.os_mock.stop)

    def test_provided_extension_should_be_used(self):
        rm('file.md')
        self.os_mock.remove.assert_called_once_with('file.md')

    def test_when_extension_is_missing_then_use_default_one(self):
        rm('file')
        self.os_mock.remove.assert_called_once_with('file.txt')
