# coding: utf-8
from unittest import mock
m = mock.Mock()
m()
m.assert_called_once_with()
m.call_args
mock.call(2, 3, a=3)
m(2, 3, a=3)
m.call_args
m.call_args == mock.call(2, 3, a=3)
m.return_value = 3
m()
m.side_effect = KeyError
m()
m.side_effect = [1, 2, 3]
m()
m()
m()
m()
m.side_effect = lambda x: x+2
m(3)
m(10)
m.assert_called_once_with(10)
m.assert_called_with(10)
m.assert_called_with(3)
m.assert_any_call(3)
m.assert_not_called()
get_ipython().magic('pinfo m.assert_not_called')
m.call_args
m.call_args_list
m.called
m.foo
m.foo.bar.spam
m.foo.bar.spam()
m.remove('file.txt')
m['a']
mm = mock.MagicMock()
mm['a']
m['a']
len(mm)
len(m)
list(mm)
list(m)
42 in mm
42 in m
dumb_os_mock = mock.Mock()
dumb_os_mock.remove('file')
dumb_os_mock.remoev('file')
dumb_os_mock.remove.asse_called_with('another file')
import os
os_mock = mock.Mock(spec=os)
os_mock.remove('file')
os_mock.remoev('file')
m = mock.Mock(specs=os)
m.specs
m = mock.Mock(foo=2, bar=3)
m.foo
m.bar
os_mock.asse_called_once_with()
os_mock.remove.asse_called_once_with()
os_mock.remove.spec = os.remove
os_mock.remove.asse_called_once_with()
os_mock.remove = mock.Mock(spec=os.remove)
os_mock.remove.asse_called_once_with()
os_mock.rm.add_spec(os.remove)
dumb_os_mock.rm.add_spec(os.remove)
dumb_os_mock.rm.asse_called_with()
get_ipython().magic('pinfo dumb_os_mock.rm.add_spec')
class Something:
    def __init__(self):
        self.foo = 42
        
something_mock = mock.Mock(spec=Something)
something_mock.foo
something_mock.foo = 42
something_mock.foo
with mock.patch('__main__.open') as open_mock:
    with open('foo', 'r') as s:
        s.read()
        
open_mock.call_args_list
open_mock.mock_calls
os_mock.mock_calls
open_mock.assert_called_once_with(mock.ANY, 'r')
get_ipython().magic('save session')
get_ipython().magic('save session *')
get_ipython().magic('save session 1-200')
