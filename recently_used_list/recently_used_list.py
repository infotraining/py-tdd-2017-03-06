from collections import deque


class RecentlyUsedList:
    def __init__(self, bound=None):
        self._list = deque()
        self.bound = bound

    def append(self, elem):
        try:
            self._list.remove(elem)
        except ValueError:
            pass
        if self.bound is not None and len(self._list) == self.bound:
            self._list.pop()
        self._list.appendleft(elem)

    def __len__(self):
        return len(self._list)

    def __getitem__(self, key):
        return self._list[key]

    def __str__(self):
        return str(self._list)

    def __repr__(self):
        return 'RecentlyUsedList({})'.format(repr(list(self._list)))
