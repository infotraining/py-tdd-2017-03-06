import unittest
from recently_used_list import RecentlyUsedList


class WhenUsingANewRecentlyUsedList(unittest.TestCase):
    def setUp(self):
        self.item = 'item'
        self.list = RecentlyUsedList()

    def test_list_should_be_empty(self):
        self.assertEqual(len(self.list), 0)

    def test_list_should_resize_after_inserting_an_item(self):
        self.list.append(self.item)
        self.assertEqual(len(self.list), 1)

    def test_list_should_store_inserted_item_at_index_zero(self):
        self.list.append(self.item)
        self.assertIs(self.list[0], self.item)


class RecentlyUsedListBaseTest(unittest.TestCase):
    bound = None
    items = ...

    def setUp(self):
        self.list = RecentlyUsedList(bound=self.bound)
        for item in self.items:
            self.list.append(item)


class WhenUsingPopulatedUnboundedRecentlyUsedList(RecentlyUsedListBaseTest):
    items = ['first', 'second', 'third']

    def test_list_should_return_number_of_elements(self):
        self.assertEqual(len(self.list), 3)

    def test_list_should_store_items_in_LIFO_order(self):
        self.assertSequenceEqual(self.list, list(reversed(self.items)))

    def test_items_should_be_looked_up_by_index(self):
        for i, item in enumerate(reversed(self.items)):
            self.assertIs(self.list[i], item)


class WhenUsingPopulatedNotfullRecentlyUsedList(
        WhenUsingPopulatedUnboundedRecentlyUsedList):
    bound = 5


class WhenInsertingADuplicateIntoUnboundedRecentlyUsedList(RecentlyUsedListBaseTest):
    items = ['first', 'second', 'third', 'second']

    def test_should_store_unique_items(self):
        items = list(self.list)
        unique_items = set(items)
        items_are_unique = len(items) == len(unique_items)
        self.assertTrue(items_are_unique)

    def test_should_move_item_to_the_front(self):
        expected_order = ['second', 'third', 'first']
        self.assertSequenceEqual(self.list, expected_order)


class WhenInsertingADuplicateIntoNotfullRecentlyUsedList(
        WhenInsertingADuplicateIntoUnboundedRecentlyUsedList):
    bound = 5


class WhenInsertingAnItemToFullRecentlyUsedList(RecentlyUsedListBaseTest):
    bound = 3
    items = ['first', 'second', 'third']

    def test_should_not_change_the_size(self):
        old_size = len(self.list)
        self.list.append('fourth')
        new_size = len(self.list)
        self.assertEqual(old_size, new_size)

    def test_should_contain_the_new_element(self):
        item = 'fourth'
        self.list.append(item)
        self.assertIn(item, self.list)

    def test_should_drop_the_last_item_on_overflow(self):
        self.list.append('fourth')
        self.assertNotIn('first', self.list)


if __name__ == "__main__":
    unittest.main()