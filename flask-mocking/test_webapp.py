import unittest

import mock

from webapp import home


# HomeViewTest -- before refactoring

class HomeViewTest(unittest.TestCase):
    @mock.patch('webapp.request')
    def test_get_request(self, request_mock):
        request_mock.method = 'GET'

        html = home()

        self.assertIn('<form', html)

    @mock.patch('webapp.request')
    def test_post_request(self, request_mock):
        request_mock.method = 'POST'
        request_mock.form = dict(number=42)

        html = home()

        self.assertIn('42+1 = 43', html)

# HomeViewTest -- after refactoring

class HomeViewTest(unittest.TestCase):
    def setUp(self):
        request_patcher = mock.patch('webapp.request')
        self.request_mock = request_patcher.start()
        self.addCleanup(request_patcher.stop)

    def test_get_request(self):
        self.request_mock.method = 'GET'

        html = home()

        self.assertIn('<form', html)

    def test_post_request(self):
        self.request_mock.method = 'POST'
        self.request_mock.form = dict(number=42)

        html = home()

        self.assertIn('42+1 = 43', html)


if __name__ == "__main__":
    unittest.main()
