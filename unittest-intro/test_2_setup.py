import unittest

class SetUpTests(unittest.TestCase):
    def test_pass(self):
        print('test_pass')

    def test_error(self):
        print('test_error')
        raise Exception

    def test_fail(self):
        print('test_fail')
        self.fail()

    def setUp(self):
        print('setUp')

    def tearDown(self):
        print('tearDown')


class InvalidSetUp(unittest.TestCase):
    def setUp(self):
        self.stream = open('test_2_setup.py', 'r')
        print('setUp')
        raise Exception

    def tearDown(self):
        self.stream.close()
        print('tearDown')

    def test_one(self):
        print('test_one')


class BetterSetUp(unittest.TestCase):
    def setUp(self):
        self.stream = open('test_2_setup.py', 'r')
        self.addCleanup(lambda: print('Stream close'))
        self.addCleanup(self.stream.close)
        self.addCleanup(lambda: print('Closing stream'))
        print('setUp')
        raise Exception

    def test_one(self):
        print('test_one')

    def tearDown(self):
        print('tearDown')


class SetUpClass(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print('setUpClass')

    def setUp(self):
        print('setUp')

    def test_one(self):
        print('test_one')

    def test_two(self):
        print('test_two')

    def tearDown(self):
        print('tearDown')

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')


def setUpModule():
    print('setUpModule')


def tearDownModule():
    print('tearDownModule')
