import unittest
import sys


class Skipping(unittest.TestCase):
    @unittest.skip('demonstrating skipping')
    def test_nothing(self):
        self.fail('should not be executed')

    @unittest.skipUnless(sys.platform.startswith('win'), 'requires Windows')
    def test_windows_support(self):
        pass

    # @unittest.skipIf()

    @unittest.expectedFailure
    def test_expected_failure(self):
        self.fail('should be executed')

    @unittest.expectedFailure
    def test_unexpected_success(self):
        pass
