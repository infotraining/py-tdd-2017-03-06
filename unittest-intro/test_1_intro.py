import unittest

def factorial(n):
    if not isinstance(n, int):
        raise TypeError('Argument must be int')
    
    factorial = 1
    for i in range(2, n+1):
        factorial *= i
    return factorial

class Factorial(unittest.TestCase):
    def test_factorial_of_one(self):
        got = factorial(1)
        expected = 2
        self.assertEqual(got, expected)

    def test_factorial_of_two(self):
        self.assertEqual(factorial(2), 2)

    def test_factorial_of_three(self):
        self.assertEqual(factorial(3), 6)

    # def test_error(self):
    #     raise ValueError()

    def test_raises_TypeError_for_invalid_argument(self):
        with self.assertRaises(TypeError):
            factorial(2.5)

    def test_error_message_when_passing_invalid_argument(self):
        with self.assertRaises(TypeError) as cm:
            factorial(2.5)
        self.assertEqual(cm.exception.args[0], 'Argument must be int')
        

if __name__ == "__main__":
    unittest.main()
