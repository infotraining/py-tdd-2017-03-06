import unittest


class Assertions(unittest.TestCase):
    def test(self):
        self.assertEqual(2, 2)
        self.assertTrue(['non-empty'])
        self.assertFalse([])

        a = {}
        b = {}
        self.assertEqual(a, b)  # a == b
        self.assertIsNot(a, b)  # a is not b

        self.assertIsNone(None)
        self.assertIsNotNone([])

        self.assertIn('bar', ['bar', 'foo'])  # a in b
        self.assertIsInstance([], list)  # isinstance(a, b)

    def test_fail_immediatelly(self):
        if a == b and c.d() == e:
            self.fail('reason')

    def test_error(self):
        raise Exception

    def test_standard(self):
        a = 2
        b = 3
        self.assertEqual(a, b)

    def test_assert(self):
        a = 2
        b = 3
        assert a == b
