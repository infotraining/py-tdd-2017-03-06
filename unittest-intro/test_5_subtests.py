import unittest


class SubTests(unittest.TestCase):
    def test_subtest(self):
        for a, b, expected in [
            (0, 0, 0),
            (2, 3, 5),
            (1, 1, 3),
            (3, 3, 5.99),
            ('a', 'b', 'ab'),
        ]:
            self.setUp()
            with self.subTest(a=a, b=b, expected=expected):
                print('subtest')
                self.assertEqual(a + b, expected)
            self.tearDown()

    def setUp(self):
        print('setUp')

    def tearDown(self):
        print('tearDown')
